import express, { json, urlencoded } from "express";
import { port } from "./src/config/config.js";
import connectDB from "./src/connectDB/connectDB.js";
import errorHandler from "./src/helper/errorHandler.js";
import cors from "cors";
import landlordRouter from "./src/router/landlordRouter.js";

let expressApp = express();
expressApp.use(json());
expressApp.use(cors());
expressApp.use(urlencoded({ extended: true }));
expressApp.use(express.static("./public"));
expressApp.use("/landlords", landlordRouter);
expressApp.use(errorHandler);
connectDB();

expressApp.listen(port, () => {
  console.log(`Express App is listening at port ${port}`);
});
