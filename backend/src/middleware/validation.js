const validation = (schema) => (req, res, next) => {
  let { error } = schema.validate(req.body);
  if (error) {
    let err = new Error(error.details[0].message);
    throw err;
  } else {
    next();
  }
};

export default validation;
