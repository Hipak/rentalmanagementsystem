import { config } from "dotenv";
import expressAsyncHandler from "express-async-handler";
import successResponse from "../helper/successResponse.js";
import { secretKey } from "../config/config.js";
import jsonwebtoken from "jsonwebtoken";


config();
export const verifyToken = expressAsyncHandler(async (req, res, next) => {
  const token = req.body.token || req.query.token || req.headers["authorization"];

  if (!token) {
    successResponse({
      res,
      message: "A token is required for authentication",
    });
      return;
  }
  try {
    const descode = jsonwebtoken.verify(token, secretKey);
    req.user = descode;
  } catch (error) {
    successResponse({
      res,
      message: error.message,
    });
    return ;
    }
    return next();
});
