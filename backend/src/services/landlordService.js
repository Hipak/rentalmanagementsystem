import { Landlord } from "../schema/mainModel.js";

export const createLandlordServices = async ({ body }) =>
  await Landlord.create(body);
export const readLandlordDetailsServices = async ({ id }) =>
  await Landlord.findById(id);
export const updateLandlordDetailsServices = async ({ id, body }) =>
  await Landlord.findByIdAndUpdate(id, body, {
    new: true,
  });
export const deleteLandlordDetailsServices = async ({ id }) =>
  await Landlord.findByIdAndDelete(id);
export const readAllLandlordServices = async ({ skip, limit, sort }) =>
  await Landlord.find({}).skip(skip).limit(limit).select("").sort(sort);

