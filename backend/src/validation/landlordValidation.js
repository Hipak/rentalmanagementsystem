import Joi from "joi";
import { genderEnum } from "../constant/constant.js";
const landlordValidation = Joi.object()
  .keys({
    firstName: Joi.string().required(),
    middleName: Joi.string().allow(null, ""),
    lastName: Joi.string().required(),
    email: Joi.string().email().required(),
    phoneNumber: Joi.string().allow(null, ""),
    password: Joi.string()
      .required()
      .min(8)
      .max(128)
      .custom((value, msg) => {
        if (
          value.match(
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/
          )
        )
          return true;
        else
          return msg.message(
            "Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character and cannot contain space."
          );
      }),
    address: Joi.string().required(),
    dob: Joi.date().allow(null, ""),
    gender: Joi.string().valid(...genderEnum),
  })
  .unknown(false);

export default landlordValidation;
