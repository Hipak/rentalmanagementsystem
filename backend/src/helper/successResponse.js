import { HttpStatus } from "../config/config.js";

const successResponse = ({ result, status, message, res }) => {
  let _result = result || null;
  let _status = status || HttpStatus.OK;
  let _message = message || "Authenticated"
  res.status(_status).json({
    success: true,
    result: _result,
    message: _message,
  });
};

export default successResponse;
