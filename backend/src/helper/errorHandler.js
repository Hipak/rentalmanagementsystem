import { HttpStatus } from "../config/config.js";

const errorHandler = (err, req, res, next) => {
  const statusCode = err.statusCode || HttpStatus.INTERNAL_SERVER_ERROR;
  const message = err?.message || "Internal server error";
  res.status(statusCode).json({
    success: false,
    message,
  });
};
export default errorHandler;
