import { Router } from "express";
import upload from "../middleware/uploadFiles.js";
import { createFile, createFiles } from "../controller/fileController.js";

const fileRouter = Router();
fileRouter.route("/single").post(upload.single("file"), createFile);
fileRouter.route("/multiple").post(upload.array("files", 2), createFiles);
export default fileRouter;
