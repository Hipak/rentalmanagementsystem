import { Schema } from "mongoose";
import { genderEnum } from "../constant/constant.js";
const landlordRegisterSchema = Schema(
  {
    firstName: {
      type: String,
      required: [true, "Please enter your firstName"],
      trim: true,
    },
    middleName: {
      type: String,
      required: false,
      trim: true,
    },
    lastName: {
      type: String,
      required: [true, "Please enter your lastName"],
      trim: true,
    },
    email: {
      type: String,
      required: [true, "Please enter your email"],
      trim: true,
      unique:true
    },
    phoneNumber: {
      type: String,
      required: [true, "Please enter your phone number"],
      trim: true,
    },
    password: {
      type: String,
      required: [true, "Please enter your password"],
      trim: true,
    },
    address: {
      type: String,
      required: [true, "Please enter your address"],
      trim: true,
    },
    dob: {
      type: Date,
      required: false,
      trim: true,
    },
    gender: {
      type: String,
      required: true,
      trim: true,
      enum: {
        values: genderEnum,
        message: (enumValue) => {
          return `${enumValue.value} is not valid enum`;
        },
      },
    },
    profilePicture: {
      type: String,
      trim: true,
    },
    // citizenshipFrontPhoto: {
    //   type: String,
    //   required: [true, "Please submit your citizenship front photo"],
    //   trim: true,
    // },
    // citizenshipBackPhoto: {
    //   type: String,
    //   required: [true, "Please submit your citizenship back photo"],
    //   trim: true,
    // },
  },
  { timestamps: true }
);
export default landlordRegisterSchema;
