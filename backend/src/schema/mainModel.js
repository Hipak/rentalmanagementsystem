import { model } from "mongoose";
import landlordRegisterSchema from "./landlordRegisterSchema.js";

export const Landlord = model("Landlord", landlordRegisterSchema);
