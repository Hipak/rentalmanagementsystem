import expressAsyncHandler from "express-async-handler";
import successResponse from "../helper/successResponse.js";
import { HttpStatus, secretKey } from "../config/config.js";
import { compareHashCode } from "../utils/hashing.js";
import { Landlord } from "../schema/mainModel.js";
import { generateToken } from "../utils/secretKey.js";
import {
  createLandlordServices,
  deleteLandlordDetailsServices,
  readAllLandlordServices,
  readLandlordDetailsServices,
  updateLandlordDetailsServices,
} from "../services/landlordService.js";
import { generateHashCode } from "../utils/hashing.js";

export const createLandlord = expressAsyncHandler(async (req, res) => {
  let hashPassword = await generateHashCode({ text: req.body.password });
  let body = {
    ...req.body,
    password: hashPassword, 
  };
  let result = await createLandlordServices({ body: body });
  console.log("Landlord Created Successfully!!");
  successResponse({ res, status: HttpStatus.OK, result });
});

export const readLandlordDetails = expressAsyncHandler(async (req, res) => {
  let result = await readLandlordDetailsServices({ id: req.params.id });
  console.log(
    `Details of ${result.firstName} ${result.middleName} ${result.lastName}`
  );
  successResponse({
    res,
    status: HttpStatus.OK,
    result,
  });
});

export const updateLandlordDetails = expressAsyncHandler(async (req, res) => {
 
  let result = await updateLandlordDetailsServices({
    id: req.params.id,
    body: req.body,
  });
  console.log(body);
  successResponse({
    res,
    status: HttpStatus.CREATED,
    result,
    message
  });
});

export const deleteLandlordDetails = expressAsyncHandler(async (req, res) => {
  let result = await deleteLandlordDetailsServices({ id: req.params.id });
  console.log("Landlord Data Deleted Successfully");
  successResponse({
    res,
    status: HttpStatus.OK,
    result,
  });
});

export const readAllLandlords = expressAsyncHandler(async (req, res) => {
  let page = Number(req.query.page) || 1;
  let limit = Number(req.query.limit) || 100;
  let skip = (page - 1) * limit;
  let sort = req.query.sort || "";

  let result = await readAllLandlordServices({ skip, limit, sort });
  console.log("All Data are retrieved");
  successResponse({
    res,
    status: HttpStatus.OK,
    result,
  });
});

export const loginLandlord = expressAsyncHandler(async (req, res, next) => {
  let email = req.body.email;
  let password = req.body.password;

  let user = await Landlord.findOne({ email: email });

  if (user) {
    let isValidPassword = await compareHashCode({
      text: password,
      hashCode: user.password,
    });

    if (isValidPassword) {
      let info = {
        id: user._id,
        email: user.email,
      };
      let expiryInfo = {
        expiresIn: "365d",
      };

      let token = await generateToken({
        info: info,
        secretKey: secretKey,
        expiryInfo: expiryInfo,
      });

      successResponse({
        res,
        status: HttpStatus.OK,
        message: "Login Success",
        result: {...info,token},
      });
    } else {
      let err = new Error("Invalid email or password");
      err.statusCode = HttpStatus.UNAUTHORIZED;
      throw err;
    }
  } else {
    let err = new Error("Invalid email or password");
    err.statusCode = HttpStatus.UNAUTHORIZED;
    throw err;
  }
  // console.log(`User : ${user}`);
});

// Test Login
export const testLogin = expressAsyncHandler(async (req, res) => {
  successResponse({
    res,
    status: HttpStatus.OK,
    message: "Authenticated",
    result:"Authentication Success"
  })
})

// Update Password Method
export const updatePassword = expressAsyncHandler(async (req, res) => {
  const user_id = req.body.user_id
  const password = req.body.password
  console.log("UP",user_id)
  const result = await Landlord.findOne({ _id: user_id })
  if (result) {
    const updatedPassword = await generateHashCode({ text: password })
    console.log("$$", updatedPassword)
    
    let result = await Landlord.findByIdAndUpdate({ _id: user_id }, { $set: { password: updatedPassword } });
    
    successResponse({
      res,
      status: HttpStatus.OK,
      result:"Password Updated",
      message: "Your Password has been updated",
    });
  }

  else {
    let err = new Error("Invalid email or password");
    err.statusCode = HttpStatus.UNAUTHORIZED;
    throw err;
  }
})
