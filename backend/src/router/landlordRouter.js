import { Router } from "express";

import {
  createLandlord,
  deleteLandlordDetails,
  loginLandlord,
  readAllLandlords,
  readLandlordDetails,
  testLogin,
  updateLandlordDetails,
  updatePassword,
} from "../controller/landlordController.js";
import validation from "../middleware/validation.js";
import landlordValidation from "../validation/landlordValidation.js";
import { verifyToken } from "../middleware/auth.js";

const landlordRouter = Router();

landlordRouter
  .route("/")
  .post(validation(landlordValidation), createLandlord)
  .get(readAllLandlords);

landlordRouter.route("/login").post(loginLandlord);
landlordRouter.route("/test").get(verifyToken,testLogin);

landlordRouter
  .route("/:id")
  .get(readLandlordDetails)
  .patch(validation(landlordValidation), updateLandlordDetails)
  .delete(deleteLandlordDetails);

// update password route
landlordRouter.route('/update-password')
.post(updatePassword)

export default landlordRouter;
